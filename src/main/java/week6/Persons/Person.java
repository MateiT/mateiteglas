package week6.Persons;

import java.util.Objects;

public class Person {

    private String CNP;
    private String name;
    private int age;

    public Person(final String CNP, final String name, int age) {
        this.CNP = CNP;
        this.name = name;
        this.age = age;
    }


    public String getCNP() {
        return CNP;
    }

    public void setCNP(String CNP) {
        this.CNP = CNP;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }


    @Override
    public String toString() {
        return "Persons.Person{" +
                "CNP='" + CNP + '\'' +
                ", name='" + name + '\'' +
                ", age=" + age +
                '}';
    }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Person person = (Person) o;
        return age == person.age && Objects.equals(CNP, person.CNP) && Objects.equals(name, person.name);
    }

    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + name.hashCode();
        result = 31 * result + age;
        result = 31 * result + CNP.hashCode();
        return result;
    }

}



