package week6.Hobbies;


public abstract class Hobbies {

    private String hobbyName;
    private int perWeekFrequency;
    public Address address;


    public Hobbies(String hobbyName, int perWeekFrequency) {
        this.hobbyName = hobbyName;
        this.perWeekFrequency = perWeekFrequency;

    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "Hobbies{" +
                "hobbyName='" + hobbyName + '\'' +
                ", perWeekFrequency=" + perWeekFrequency +
                '}';
    }

    //    @Override
//    public boolean equals(Object o) {
//        if (this == o){
//            return true;
//        }
//        if (o == null || getClass() != o.getClass()){
//            return false;
//        }
//        Hobbies hobbies = (Hobbies) o;
//        return perWeekFrequency == hobbies.perWeekFrequency && Objects.equals(hobbyName, hobbies.hobbyName) && Objects.equals(address, hobbies.address);
//    }
//
//    @Override
//    public int hashCode() {
//        return Objects.hash(hobbyName);
//    }
}


