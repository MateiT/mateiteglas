package week6.Hobbies;

public class Address {

    private String region;
    private String country; //this initialized parameters that are needed to define users
    private String street;
    private int streetNo;



    public Address(String region, String country, String street, int streetNo) { //this is a constructor function that ensures that we can never create a new user without giving it a type
        this.region = region;
        this.country = country;
        this.street = street;
        this.streetNo = streetNo;


    }

    public String getRegion() {
        return region;
    }

    public String getCountry() {
        return country;
    }

    public String getStreet() {
        return street;

    }public int getStreetNo() {
        return streetNo;
    }

    @Override
    public String toString() {
        return region + " " + country+ " " + street;

    }

}
