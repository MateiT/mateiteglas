package week6;


import week6.Hobbies.*;
import week6.Persons.Hired;
import week6.Persons.Person;
import week6.Persons.Student;
import week6.Persons.Unemployed;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        //initializing people and hobbies

   Person person1 = new Hired("123456", "Jack", 35);
   Person person2 = new Unemployed("130496", "Juliet", 28);
   Person person3 = new Student("144497", "James", 29);

   Address address1 =new Address("NorthAmerica", "UnitedStates", "KimbleyStreet", 101);
   Address address2 =new Address("NorthAmerica", "UnitedStates", "AmbroseStreet", 33);
   Address address3 =new Address("NorthAmerica", "UnitedStates", "RepublicStreet", 290);


   Hobbies hobby1 = new HorseBackRiding("horse-riding", 4);
   Hobbies hobby2 = new Volleyball("volleyball", 4);
   Hobbies hobby3 = new Squash("squash", 4);

        hobby1.setAddress(address1);
        hobby2.setAddress(address2);
        hobby3.setAddress(address3);


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//sorting people

        Comparator<Person> personComparator = new Comparator<Person>() {
            @Override
            public int compare(Person o1, Person o2) {

                if (o1.getName().equals(o2.getName())) { //checks if names are the same
                    return o1.getAge()-o2.getAge(); //if return is +ve, 2nd object bigger, if -ve, 1st object bigger if 0, objects equal
                }
                return o1.getName().compareTo(o2.getName()); //if names are the same, we then compare age, returning an integer, as above
            }
        };

    Set<Person> sortedPersons = new TreeSet<>(personComparator); //comparator used in the same class as TreeSet

    sortedPersons.add(person1);
    sortedPersons.add(person2);
    sortedPersons.add(person3);

        System.out.println(sortedPersons);

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//attributing hobbies

        Map<Person, List<Hobbies>> personMap = new HashMap<>();

        personMap.put(person1, Arrays.asList(hobby1, hobby2, hobby3));
        personMap.put(person2, Arrays.asList(hobby3, hobby1));
        personMap.put(person3, Arrays.asList(hobby2));


        System.out.println(personMap);


    }


}
