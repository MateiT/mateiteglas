package week7.Calculator.Calculator.Calculator;

import week7.Calculator.Calculator.Calculator.UnitType;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in); //System.in is a standard input stream, used to input whatever string we want (such as in the example in line 17).
        System.out.println("Conditions -> space only between the operators and no space between the value and the unit type"); //describes how string has to be inputted in order for method at line 19 to work
        System.out.println("Example -> 5mm + 3cm + 3m - 2dm");
        System.out.print("Enter an expression to evaluate it: ");

        String example= sc.nextLine().trim().toUpperCase(); //deletes spaces at beggining and end of input string and converts the units Upper case

        System.out.print("Enter the unit type of the result (mm,cm,dm,m,km):"); //asks to enter the unit-type in which you want the example to be in

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


        String unitType = sc.nextLine().trim().toUpperCase(); //deletes spaces at beggining and end of input string and converts the units Upper case
        UnitType resultUnitType = Calculator.getUnitType(unitType);

//        example = example.toUpperCase(Locale.ROOT); //

        String[] split = example.split(" "); //splits string into an array of Strings, where String terms can be: 1)values+units, 2) as operation signs

        List<Double> values = new ArrayList<>(); //array list for storing values
        List<String> operations = new ArrayList<>(); //array list for storing operation signs

        //convert values and get operators by going through the newly created split string
        for (int i = 0; i < split.length; i++) {
            if (i % 2 == 0) {
                values.add(Calculator.convert(Calculator.getUnitType(split[i]), resultUnitType, Calculator.getValue(split[i]))); //adds even-positioned elements from 'split' to 'values' array list
            } else {
                operations.add(split[i]); //adds odd-positioned elements from 'split' to 'operations' array list
            }
        }

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        // total is the result. initial value of total is the first value in the 'values' array list
        double total = values.get(0); //initializes a variable called total which gets the first position of the values array
        for (int i = 0; i < operations.size(); i++) {
            if (operations.get(i).equals("+")) {
                total = total + values.get(i + 1); //updates the total variable by ADDING the NEXT element within values array, if element of 'operations' is a plus
            } else {
                total = total - values.get(i + 1); //updates the total variable by SUBTRACTING the NEXT element within values array, if element of 'operations' is a minus
            }
        }

        System.out.println(example + " = " + total + resultUnitType.name());

    }
}