package week7.Calculator.Calculator.Calculator;


public class Calculator {

    public static double convert(UnitType from, UnitType to, double value) {
        if (from == UnitType.MM) {
            if (to == UnitType.MM){
                return value;
            }
            if (to == UnitType.CM){
                return value/10;
            }
            if (to == UnitType.DM){
                return value/100;
            }
            if (to == UnitType.M){
                return value/1000;
            }
            if (to == UnitType.KM){
                return value/1000000;
            }

        }

        if (from == UnitType.CM) {
            if (to == UnitType.CM){
                return value;
            }
            if (to == UnitType.MM){
                return value*10;
            }
            if (to == UnitType.DM){
                return value/10;
            }
            if (to == UnitType.M){
                return value/100;
            }
            if (to == UnitType.KM){
                return value/100000;
            }

        }

        if (from == UnitType.DM) {
            if (to == UnitType.DM){
                return value;
            }
            if (to == UnitType.MM){
                return value*100;
            }
            if (to == UnitType.CM){
                return value*10;
            }
            if (to == UnitType.M){
                return value/10;
            }
            if (to == UnitType.KM){
                return value/10000;
            }

        }

        if (from == UnitType.M) {
            if (to == UnitType.M){
                return value;
            }
            if (to == UnitType.MM){
                return value*1000;
            }
            if (to == UnitType.CM){
                return value*100;
            }
            if (to == UnitType.DM){
                return value*10;
            }
            if (to == UnitType.KM){
                return value/1000;
            }

        }

        if (from == UnitType.KM) {
            if (to == UnitType.KM){
                return value;
            }
            if (to == UnitType.MM){
                return value/1000000;
            }
            if (to == UnitType.CM){
                return value/100000;
            }
            if (to == UnitType.DM){
                return value/10000;
            }
            if (to == UnitType.M){
                return value/1000;
            }
        }
        return 0;
    }

    public static int getValue(String value) {
        if (value.contains("MM"))
            return Integer.parseInt(value.split("MM")[0]);
        if (value.contains("CM"))
            return Integer.parseInt(value.split("CM")[0]);
        if (value.contains("DM"))
            return Integer.parseInt(value.split("DM")[0]);
        if (value.contains("KM"))
            return Integer.parseInt(value.split("KM")[0]);
        if (value.contains("M"))
            return Integer.parseInt(value.split("M")[0]);

        return 0;

    }

    public static UnitType getUnitType (String value) {
        if (value.contains("MM"))
            return UnitType.MM;
        if (value.contains("CM"))
            return UnitType.CM;
        if (value.contains("DM"))
            return UnitType.DM;
        if (value.contains("KM"))
            return UnitType.KM;
        if (value.contains("M"))
            return UnitType.M;


        return null;
    }
}