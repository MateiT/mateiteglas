package week5.VehicleProject.Car.Audi;


import week5.VehicleProject.Car.Car.Car;

public class A5 extends Car implements IAudi {

    String carColour;
    public String audiCarName; //used to name the phone model
    public String audiChasisNo;
    public int audiGear;
    private String fuelType;

    private int consumptionA5;


    private static int getAudiFuelTankSize() {
        return audiFuelTankSize;
    }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


    public A5(String carColour, String audiCarName, String audiChasisNo, String fuelType) {
        super(6, 1, 4, 0.3);
        this.carColour = carColour;
        this.audiCarName = audiCarName;
        this.audiChasisNo = audiChasisNo;
        this.fuelType = fuelType;
        this.audiGear = gear;

    }

    @Override
    public void reFillTank (double availableFuel) {
        if (this.availableFuel + availableFuel > audiFuelTankSize + 7) {
            this.availableFuel = audiFuelTankSize + 7;
            System.out.println("can't fill with more than maximum A5 tank capacity");
            System.out.println("tank full");
        }
        else {
            this.availableFuel = availableFuel + this.availableFuel;
            System.out.println("available fuel in car after refill is: " + this.availableFuel);
        }

    }


    @Override
    public String toString() {
        return "A5{" +
                "carColour='" + carColour + '\'' +
                ", audiCarName='" + audiCarName + '\'' +
                ", audiChasisNo='" + audiChasisNo + '\'' +
                ", audiGear=" + audiGear +
                ", fuelType='" + fuelType + '\'' +
                ", consumptionA5=" + consumptionA5 +
                '}';
    }
}
