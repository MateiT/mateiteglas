package week5.VehicleProject.Car.Car;

public abstract class Vehicle {

    protected double availableFuel;
    protected double consumptionPer100Km; //consumption for 15'' tires and in first gear
    protected double consumption; //consumption for 15'' tires and in first gear


    public Vehicle(double consumptionPer100Km) {
        this.consumptionPer100Km = consumptionPer100Km;
        this.consumption = consumptionPer100Km;
    }

//    public Vehicle() {
//
//    }

    public void reFillTank (double availableFuel) {
        this.availableFuel = availableFuel;
    }


    public void start(int availableFuel) {

        this.consumptionPer100Km = consumption; //first Gear 15" tires

        System.out.println("the consumption has been reset to: " + consumptionPer100Km);

        if (availableFuel < 1) {
            System.out.println("not enough fuel to start car");

        } else {
            System.out.println("car started");
        }
    }

    public void drive(int distance) {

            double consumptionLiters = ((consumptionPer100Km * distance)/100);
            System.out.println("available fuel: " + availableFuel + ",trying to drive " + distance + ",consuming" + consumptionLiters);
            System.out.println("consumption per 100 km: " + consumptionPer100Km);
            if (this.availableFuel<consumptionLiters) {
                System.out.println("not enough to drive this distance");
                return;
            }

            this.availableFuel = availableFuel - consumptionLiters;

        System.out.println("available fuel after this drive is: " + this.availableFuel);
    }


    public void stop() {

        System.out.println("vehicle has stopped");

    }

}
