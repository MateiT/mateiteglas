package week5.VehicleProject.Car;


import week5.VehicleProject.Car.Audi.A5;
import week5.VehicleProject.Car.Car.Car;

public class Main {

    public static void main(String[] args) {

        Car car1 = new A5("Black", "A5", "123456", "Diesel"); //Car already extends vehicle, no need to instantiate vehicicle as they are already available in Car

        car1.reFillTank(100);

        System.out.println(car1);

        car1.start(100);

        car1.drive(100);

        car1.shift( 2);

        car1.drive(200);

        car1.shift(3);

        car1.drive(300);

        car1.shift(4);

        car1.drive(100);

        car1.shift(5);

        car1.drive(150);

        car1.shift(6);

        car1.drive(110);

        car1.shift(7);

        car1.stop();

    }
}
