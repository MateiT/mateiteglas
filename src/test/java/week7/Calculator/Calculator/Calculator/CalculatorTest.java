package week7.Calculator.Calculator.Calculator;


import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class CalculatorTest {

//test for convert function

    //test for MM

    @Test
    void convertMM_toM_success() {
        assertTrue(Calculator.convert(UnitType.MM, UnitType.M, 10000) == 10); //asserts if convert can convert from MM input to M output by giving it 10000mm measurement

    }


    @Test
    void convertMM_toM_fail() {
        assertFalse(Calculator.convert(UnitType.MM, UnitType.M, 10000) == 1);

    }

    //test for CM

    @Test
    void convertCM_toM_success() {
        assertTrue(Calculator.convert(UnitType.CM, UnitType.M, 1000) == 10); //asserts if convert can convert from MM input to M output by giving it 10000mm measurement

    }



    @Test
    void convertCM_toM_fail() {
        assertFalse(Calculator.convert(UnitType.CM, UnitType.M, 1000) == 1);

    }

    //test for DM

    @Test
    void convertDM_toM_success() {
        assertTrue(Calculator.convert(UnitType.DM, UnitType.M, 100) == 10); //asserts if convert can convert from MM input to M output by giving it 10000mm measurement

    }



    @Test
    void convertDM_toM_fail() {
        assertFalse(Calculator.convert(UnitType.DM, UnitType.M, 100000) == 1);

    }

    //test for km





    @Test
    void convertKM_toM_fail() {
        assertFalse(Calculator.convert(UnitType.KM, UnitType.M, 100000) == 1);

    }


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//testing the getValue function

    //test for MM

    @Test
    void getValueMM_success() {
        assertTrue(Calculator.getValue("10MM") == 10);

    }



    @Test
    void getValueMM_fail() {
        assertFalse(Calculator.getValue("10MM") == 100);

    }

    //test for CM

    @Test
    void getValueCM_success() {
        assertTrue(Calculator.getValue("10CM") == 10);

    }

    @Test
    void getValueCM__fail() {
        assertTrue(Calculator.getValue("10CM") == 10);

    }

    @Test
    void getValueCM_fail() {
        assertFalse(Calculator.getValue("10CM") == 100);

    }

    //test for DM

    @Test
    void getValueDM_success() {
        assertTrue(Calculator.getValue("10DM") == 10);

    }

    @Test
    void getValueDM__fail() { //to check for fails test (disparity between outcome and assertion)
        assertTrue(Calculator.getValue("10DM") == 10);

    }

    @Test
    void getValueDM_fail() {
        assertFalse(Calculator.getValue("10DM") == 100);

    }

    //test for M

    @Test
    void getValueM_success() {
        assertTrue(Calculator.getValue("10M") == 10);

    }

    @Test
    void getValueM__fail() {
        assertTrue(Calculator.getValue("10M") == 10);

    }

    @Test
    void getValueM_fail() {
        assertFalse(Calculator.getValue("10M") == 100);

    }

    //test for KM

    @Test
    void getValueKM_success() {
        assertTrue(Calculator.getValue("10KM") == 10);

    }


    @Test
    void getValueKM_fail() {
        assertFalse(Calculator.getValue("10KM") == 100);

    }


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//test for getUnitType function

    //test for MM

    @Test
    void getUnitTypeMM_success() {
        assertTrue(Calculator.getUnitType("1000MM") == UnitType.MM);

    }

    //test for CM


    //test for DM

    @Test
    void getUnitTypeMM_false() {
        assertTrue(Calculator.getUnitType("1000DM") == UnitType.DM);

    }

    //test for M

    @Test
    void getUnitTypeCM_success() {
        assertTrue(Calculator.getUnitType("1000M") == UnitType.M);

    }

    //test for KM


}

